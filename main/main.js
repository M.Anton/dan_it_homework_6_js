function createNewUser() {
    let firstName = prompt("What is your first name?");
    let lastName = prompt("What is your last name?");

    let birthday = prompt("Enter your date of birth (dd.mm.yyyy)", "dd.mm.yyyy");


    let newUser = {
        _username: firstName,
        _surname: lastName,
        getLogin() {
            return this._username[0].toLowerCase() + this._surname.toLowerCase();
        }, 

        getAge() {
            let year = birthday.slice(-4);
            let month = birthday.slice(4,5) - 1;
            let day= birthday.slice(0,2);
            let date = new Date(year, month, day);
            let dateNow = new Date();
            let age = Math.floor( (dateNow - date) / 1000 / 60 / 60 / 24 / 365 );
            return `Your age is ${age} years old!`;
        },

  
        getPassword() {
            let year = birthday.slice(-4);
            return this._username[0].toUpperCase() + this._surname.toLowerCase() + year;
        },
    };

    Object.defineProperty(newUser, "username", {
        get(){
            console.log(`The first name is ${this._username}!`);
        },
        set(newValue){
            console.log("You are changing the value of a first name!");
            this._username = newValue;
        },
    });
    Object.defineProperty(newUser, "surname", {
        get(){
            console.log(`The last name is ${this._surname}!`);
        },
        set(newValue){
            console.log("You are changing the value of a last name!");
            this._surname = newValue;
        },
    });
    return newUser;
}

let user = createNewUser();
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());